package com.example.vaadin7.converter;

import java.math.BigDecimal;
import java.util.Locale;

import com.vaadin.data.util.converter.AbstractStringToNumberConverter;

public class StringToBigDecimalConverter extends
		AbstractStringToNumberConverter<BigDecimal> {

	@Override
	public BigDecimal convertToModel(String value,
			Class<? extends BigDecimal> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		// TODO Auto-generated method stub
		try {         
			BigDecimal bigDecimal= BigDecimal.valueOf(Double.valueOf(value));
			return bigDecimal;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConversionException(e);
		}
 	}

	@Override
	public Class<BigDecimal> getModelType() {
		// TODO Auto-generated method stub
		return BigDecimal.class;
	}

}