package com.example.vaadin7.actionlistener;

import java.math.BigDecimal;
import java.util.Date;

import com.example.vaadin7.biz.FinanceServiceFactory;
import com.example.vaadin7.biz.Position;
import com.example.vaadin7.biz.Trade;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.UserError;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window;

public class SellActionButtonActionListerner extends FieldGroupActionListerner {

	@Override
	public void buttonClick(ClickEvent event) {

		try {
			getFieldGroup().commit();
			BeanItem<Trade> tradeItem = ((BeanItem<Trade>) getFieldGroup()
					.getItemDataSource());
			Trade trade = tradeItem.getBean();
			trade.setBuyDate(new Date());
			trade.setAmount(trade.getBuyPrice().multiply(
					new BigDecimal(trade.getQuantity())));
			Position existingPosition = FinanceServiceFactory
					.getFinanceService().getPositionCalculator()
					.getExistingPosition(trade.getStockId());

			if (existingPosition == null
					|| trade.getQuantity() > existingPosition.getQuantity()) {
				event.getButton().setComponentError(
						new UserError("Quantity is Invalid"));
				return;
			}
			Notification.show("Sell " + trade.getStockName() + " At Price:"
					+ trade.getBuyPrice() + " Quantity:" + trade.getQuantity());
			FinanceServiceFactory.getFinanceService().getPositionCalculator()
					.sell(trade);

		} catch (Exception e) {
			e.printStackTrace();
			event.getButton().setComponentError(new UserError(e.toString()));
			return;
		}
		((Window) event.getComponent().getParent().getParent().getParent())
				.close();

	}
}