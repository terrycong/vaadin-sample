package com.example.vaadin7.actionlistener;

import com.example.vaadin7.TransactionWindowContentPanel;
import com.example.vaadin7.biz.Stock;
import com.example.vaadin7.biz.TransactionType;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.sqlcontainer.SQLContainer;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class BuyButtonActionListener implements ClickListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6628022264529611517L;
	private Table source;
	private Object itemId;

	public BuyButtonActionListener(Table source, Object itemId) {
		// TODO Auto-generated constructor stub
		this.source = source;
		this.itemId = itemId;
	}

	@Override
	public void buttonClick(ClickEvent event) {

		Item item = source.getItem(itemId);
		Property<String> stockIdItemProperty = item.getItemProperty("STOCK_ID");
		Property<String> stockNameItemProperty = item
				.getItemProperty("STOCK_NAME");

		Window window = new Window("Buy " + stockNameItemProperty.getValue(),
				new TransactionWindowContentPanel(new BeanItem<Stock>(
						new Stock(stockIdItemProperty.getValue().toString(),
								stockNameItemProperty.getValue().toString())),
						TransactionType.BUY));
		window.addCloseListener(new CloseListener() {
			@Override
			public void windowClose(CloseEvent e) {
				Table table = (Table) VaadinSession.getCurrent().getAttribute(
						"POSITION_TABLE");
				table.refreshRowCache();
				((SQLContainer) table.getContainerDataSource()).refresh();
			}
		});
		window.setWidth("300px");
		window.setHeight("250px");
		window.setModal(true);
		window.center();
		UI.getCurrent().addWindow(window);

	}
}