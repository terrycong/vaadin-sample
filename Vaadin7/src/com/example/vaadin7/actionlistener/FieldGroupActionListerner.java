package com.example.vaadin7.actionlistener;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.ui.Button.ClickListener;

public abstract class FieldGroupActionListerner implements ClickListener {
	private FieldGroup fieldGroup;

	public FieldGroup getFieldGroup() {
		return fieldGroup;
	}

	public void setFieldGroup(FieldGroup fieldGroup) {
		this.fieldGroup = fieldGroup;
	}
}
