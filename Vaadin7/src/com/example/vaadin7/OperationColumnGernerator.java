package com.example.vaadin7;

import com.example.vaadin7.actionlistener.BuyButtonActionListener;
import com.example.vaadin7.actionlistener.SellButtonActionListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;

public class OperationColumnGernerator implements Table.ColumnGenerator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Object generateCell(final Table source, final Object itemId,
			final Object columnId) {
		// TODO Auto-generated method stub
		HorizontalLayout layout = new HorizontalLayout();
		Button buyButton = new Button("Buy");
		Button sellButton = new Button("Sell");
		buyButton.addClickListener(new BuyButtonActionListener(source, itemId));

		sellButton
				.addClickListener(new SellButtonActionListener(source, itemId));
		layout.addComponent(buyButton);
		layout.addComponent(sellButton);
		return layout;
	}

}
