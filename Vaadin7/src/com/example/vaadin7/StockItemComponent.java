package com.example.vaadin7;

import com.example.vaadin7.biz.Stock;
import com.vaadin.annotations.AutoGenerated;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
@Deprecated
public class StockItemComponent extends CustomComponent {

	/*- VaadinEditorProperties={"grid":"RegularGrid,20","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":10} */

	@AutoGenerated
	private HorizontalLayout mainLayout;
	@AutoGenerated
	private Panel panel_2;
	@AutoGenerated
	private HorizontalLayout horizontalLayout_2;
	@AutoGenerated
	private Label priceLabel;
	@AutoGenerated
	private Label stockNameLabel;
	@AutoGenerated
	private Label stockIdLabel;
	/**
	 * The constructor should first build the main layout, set the composition
	 * root and then do any custom initialization.
	 * 
	 * The constructor will not be automatically regenerated by the visual
	 * editor.
	 */

	public StockItemComponent(Stock stock) {
		this();
		stockIdLabel.setValue(stock.getId());
		stockNameLabel.setValue(stock.getName());
		priceLabel.setValue(stock.getCurrentPrice());
//		this.setData(stock);
		
	}

	public StockItemComponent() {
		buildMainLayout();
		setCompositionRoot(mainLayout);

		// TODO add user code here
		mainLayout.setStyleName("stock_boader");
	}

	@AutoGenerated
	private HorizontalLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new HorizontalLayout();
		mainLayout.setImmediate(false);
		mainLayout.setWidth("-1px");
		mainLayout.setHeight("-1px");
		mainLayout.setMargin(false);
		
		// top-level component properties
		setWidth("-1px");
		setHeight("-1px");
		
		// panel_2
		panel_2 = buildPanel_2();
		mainLayout.addComponent(panel_2);
		
		return mainLayout;
	}

	@AutoGenerated
	private Panel buildPanel_2() {
		// common part: create layout
		panel_2 = new Panel();
		panel_2.setImmediate(false);
		panel_2.setWidth("-1px");
		panel_2.setHeight("-1px");
		
		// horizontalLayout_2
		horizontalLayout_2 = buildHorizontalLayout_2();
		panel_2.setContent(horizontalLayout_2);
		
		return panel_2;
	}

	@AutoGenerated
	private HorizontalLayout buildHorizontalLayout_2() {
		// common part: create layout
		horizontalLayout_2 = new HorizontalLayout();
		horizontalLayout_2.setImmediate(false);
		horizontalLayout_2.setWidth("100.0%");
		horizontalLayout_2.setHeight("100.0%");
		horizontalLayout_2.setMargin(false);
		
		// stockIdLabel
		stockIdLabel = new Label();
		stockIdLabel.setImmediate(false);
		stockIdLabel.setWidth("50px");
		stockIdLabel.setHeight("-1px");
		stockIdLabel.setValue("Label");
		horizontalLayout_2.addComponent(stockIdLabel);
		
		// stockNameLabel
		stockNameLabel = new Label();
		stockNameLabel.setImmediate(false);
		stockNameLabel.setWidth("200px");
		stockNameLabel.setHeight("-1px");
		stockNameLabel.setValue("Label");
		horizontalLayout_2.addComponent(stockNameLabel);
		
		// priceLabel
		priceLabel = new Label();
		priceLabel.setImmediate(false);
		priceLabel.setWidth("200px");
		priceLabel.setHeight("-1px");
		priceLabel.setValue("Label");
		horizontalLayout_2.addComponent(priceLabel);
		
		return horizontalLayout_2;
	}

}
