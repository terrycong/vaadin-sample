package com.example.vaadin7;

import java.util.List;

import com.example.vaadin7.biz.SimStockFactory;
import com.example.vaadin7.biz.Stock;
import com.example.vaadin7.dao.SQLContainerFactory;
import com.vaadin.annotations.AutoGenerated;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class SampleComponentbackup extends CustomComponent {

	/*- VaadinEditorProperties={"grid":"RegularGrid,10","showGrid":true,"snapToGrid":true,"snapToObject":true,"movingGuides":false,"snappingDistance":11} */

	@AutoGenerated
	private VerticalLayout mainLayout;
	@AutoGenerated
	private Panel panel_1;
	@AutoGenerated
	private HorizontalLayout horizontalLayout_1;
	@AutoGenerated
	private Panel rightPanel;
	@AutoGenerated
	private VerticalLayout rightPancelVerticalLayout;
	@AutoGenerated
	private Table positionTable;
	@AutoGenerated
	private Panel leftPanel;
	@AutoGenerated
	private VerticalLayout leftPancelVerticalLayout;
	@AutoGenerated
	private Table stocksTable;
	@AutoGenerated
	private Panel panel_2;
	@AutoGenerated
	private VerticalLayout verticalLayout_1;
	@AutoGenerated
	private MenuBar menuBar_1;
	/**
	 * The constructor should first build the main layout, set the composition
	 * root and then do any custom initialization.
	 * 
	 * The constructor will not be automatically regenerated by the visual
	 * editor.
	 */
	public SampleComponentbackup() {
		buildMainLayout();
		setCompositionRoot(mainLayout);

		// TODO add user code here
		stocksTable.setSelectable(true);

//		showStockButton.addClickListener(new ClickListener() {
//
//			@Override
//			public void buttonClick(ClickEvent event) {
//				Notification.show("Show Stocks1");
//				List<Stock> stockList = SimStockFactory.getSimStockList();
//				BeanItemContainer<Stock> beans = new BeanItemContainer<Stock>(
//						Stock.class);
//				beans.addAll(stockList);
//
//				stocksTable.setContainerDataSource(beans);
//				stocksTable.addGeneratedColumn("Operation",
//						new OperationColumnGernerator());
//				event.getButton().setEnabled(false);
//			}
//		});
//
//		showPostionButton.addClickListener(new ClickListener() {
//			@Override
//			public void buttonClick(ClickEvent event) {
//				positionTable.setContainerDataSource(SQLContainerFactory
//						.getSQLContainer());
//				positionTable.refreshRowCache();
//			}
//		});
		// Put table in session
		VaadinSession.getCurrent()
				.setAttribute("POSITION_TABLE", positionTable);
		//
		leftPanel.setCaption("Stocks");
		leftPanel.setStyleName("panel");
		rightPanel.setCaption("Position");
		rightPanel.setStyleName("panel");
		rightPancelVerticalLayout.setStyleName("layout");
		leftPancelVerticalLayout.setStyleName("layout");

		initMenuBar();
		stocksTable.setEditable(true);
	}

	private void initMenuBar() {
		// TODO Auto-generated method stub
		menuBar_1.addItem("Start", new Command() {

			@Override
			public void menuSelected(MenuItem selectedItem) {
				// TODO Auto-generated method stub
				Notification.show("Show Stocks1");
				List<Stock> stockList = SimStockFactory.getSimStockList();
				BeanItemContainer<Stock> beans = new BeanItemContainer<Stock>(
						Stock.class);
				beans.addAll(stockList);

				stocksTable.setContainerDataSource(beans);
				stocksTable.addGeneratedColumn("Operation",
						new OperationColumnGernerator());
				positionTable.setContainerDataSource(SQLContainerFactory
						.getSQLContainer());
				positionTable.refreshRowCache();
			}
		});
	}

	@AutoGenerated
	private VerticalLayout buildMainLayout() {
		// common part: create layout
		mainLayout = new VerticalLayout();
		mainLayout.setImmediate(false);
		mainLayout.setWidth("100%");
		mainLayout.setHeight("-1px");
		mainLayout.setMargin(true);
		
		// top-level component properties
		setWidth("100.0%");
		setHeight("-1px");
		
		// panel_2
		panel_2 = buildPanel_2();
		mainLayout.addComponent(panel_2);
		
		// panel_1
		panel_1 = buildPanel_1();
		mainLayout.addComponent(panel_1);
		
		return mainLayout;
	}

	@AutoGenerated
	private Panel buildPanel_2() {
		// common part: create layout
		panel_2 = new Panel();
		panel_2.setImmediate(false);
		panel_2.setWidth("100.0%");
		panel_2.setHeight("100.0%");
		
		// verticalLayout_1
		verticalLayout_1 = buildVerticalLayout_1();
		panel_2.setContent(verticalLayout_1);
		
		return panel_2;
	}

	@AutoGenerated
	private VerticalLayout buildVerticalLayout_1() {
		// common part: create layout
		verticalLayout_1 = new VerticalLayout();
		verticalLayout_1.setImmediate(false);
		verticalLayout_1.setWidth("100.0%");
		verticalLayout_1.setHeight("100.0%");
		verticalLayout_1.setMargin(false);
		
		// menuBar_1
		menuBar_1 = new MenuBar();
		menuBar_1.setImmediate(false);
		menuBar_1.setWidth("-1px");
		menuBar_1.setHeight("100.0%");
		verticalLayout_1.addComponent(menuBar_1);
		verticalLayout_1.setExpandRatio(menuBar_1, 1.0f);
		
		return verticalLayout_1;
	}

	@AutoGenerated
	private Panel buildPanel_1() {
		// common part: create layout
		panel_1 = new Panel();
		panel_1.setImmediate(false);
		panel_1.setWidth("100.0%");
		panel_1.setHeight("100.0%");
		
		// horizontalLayout_1
		horizontalLayout_1 = buildHorizontalLayout_1();
		panel_1.setContent(horizontalLayout_1);
		
		return panel_1;
	}

	@AutoGenerated
	private HorizontalLayout buildHorizontalLayout_1() {
		// common part: create layout
		horizontalLayout_1 = new HorizontalLayout();
		horizontalLayout_1.setImmediate(false);
		horizontalLayout_1.setWidth("-1px");
		horizontalLayout_1.setHeight("100.0%");
		horizontalLayout_1.setMargin(false);
		
		// leftPanel
		leftPanel = buildLeftPanel();
		horizontalLayout_1.addComponent(leftPanel);
		
		// rightPanel
		rightPanel = buildRightPanel();
		horizontalLayout_1.addComponent(rightPanel);
		
		return horizontalLayout_1;
	}

	@AutoGenerated
	private Panel buildLeftPanel() {
		// common part: create layout
		leftPanel = new Panel();
		leftPanel.setImmediate(false);
		leftPanel.setWidth("40.0%");
		leftPanel.setHeight("100.0%");
		
		// leftPancelVerticalLayout
		leftPancelVerticalLayout = buildLeftPancelVerticalLayout();
		leftPanel.setContent(leftPancelVerticalLayout);
		
		return leftPanel;
	}

	@AutoGenerated
	private VerticalLayout buildLeftPancelVerticalLayout() {
		// common part: create layout
		leftPancelVerticalLayout = new VerticalLayout();
		leftPancelVerticalLayout.setImmediate(false);
		leftPancelVerticalLayout.setWidth("100.0%");
		leftPancelVerticalLayout.setHeight("100.0%");
		leftPancelVerticalLayout.setMargin(false);
		
		// stocksTable
		stocksTable = new Table();
		stocksTable.setImmediate(false);
		stocksTable.setWidth("-1px");
		stocksTable.setHeight("100.0%");
		leftPancelVerticalLayout.addComponent(stocksTable);
		
		return leftPancelVerticalLayout;
	}

	@AutoGenerated
	private Panel buildRightPanel() {
		// common part: create layout
		rightPanel = new Panel();
		rightPanel.setImmediate(false);
		rightPanel.setWidth("60.0%");
		rightPanel.setHeight("100.0%");
		
		// rightPancelVerticalLayout
		rightPancelVerticalLayout = buildRightPancelVerticalLayout();
		rightPanel.setContent(rightPancelVerticalLayout);
		
		return rightPanel;
	}

	@AutoGenerated
	private VerticalLayout buildRightPancelVerticalLayout() {
		// common part: create layout
		rightPancelVerticalLayout = new VerticalLayout();
		rightPancelVerticalLayout.setImmediate(false);
		rightPancelVerticalLayout.setWidth("100.0%");
		rightPancelVerticalLayout.setHeight("100.0%");
		rightPancelVerticalLayout.setMargin(false);
		
		// positionTable
		positionTable = new Table();
		positionTable.setImmediate(false);
		positionTable.setWidth("-1px");
		positionTable.setHeight("100.0%");
		rightPancelVerticalLayout.addComponent(positionTable);
		
		return rightPancelVerticalLayout;
	}

}
