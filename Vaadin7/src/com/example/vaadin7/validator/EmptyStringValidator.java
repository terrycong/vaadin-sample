package com.example.vaadin7.validator;

import org.apache.commons.lang.StringUtils;

import com.vaadin.data.Validator;

public class EmptyStringValidator implements Validator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2792515420575630996L;

	@Override
	public void validate(Object value) throws InvalidValueException {
		if (value==null||StringUtils.isEmpty(value.toString())) {
			new EmptyValueException("can not be null or empty");
		}
	}

}
