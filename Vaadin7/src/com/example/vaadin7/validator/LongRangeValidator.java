package com.example.vaadin7.validator;

import com.vaadin.data.validator.RangeValidator;

@SuppressWarnings("serial")
public class LongRangeValidator extends RangeValidator<Long> {

	/**
	 * Creates a validator for checking that an Integer is within a given range.
	 * 
	 * By default the range is inclusive i.e. both minValue and maxValue are
	 * valid values. Use {@link #setMinValueIncluded(boolean)} or
	 * {@link #setMaxValueIncluded(boolean)} to change it.
	 * 
	 * 
	 * @param errorMessage
	 *            the message to display in case the value does not validate.
	 * @param minValue
	 *            The minimum value to accept or null for no limit
	 * @param maxValue
	 *            The maximum value to accept or null for no limit
	 */
	public LongRangeValidator(String errorMessage, Long minValue, Long maxValue) {
		super(errorMessage, Long.class, minValue, maxValue);
	}

}