package com.example.vaadin7.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;

public class ConnectionPoolHolder {
	private static JDBCConnectionPool pool;

	public ConnectionPoolHolder() {

	}

	public static JDBCConnectionPool getPool() {
		if (pool == null) {
			try {
				InputStream resource = ConnectionPoolHolder.class
						.getClassLoader()
						.getResourceAsStream("jdbc.properties");
				Properties ppts = new Properties();
				try {
					ppts.load(resource);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				pool = new SimpleJDBCConnectionPool(
						ppts.getProperty("jdbc.driver"),
						ppts.getProperty("jdbc.url"),
						ppts.getProperty("jdbc.username"),
						ppts.getProperty("jdbc.password"),2,5);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pool;
	}

}
