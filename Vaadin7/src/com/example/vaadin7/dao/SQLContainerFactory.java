package com.example.vaadin7.dao;

import java.sql.SQLException;

import com.vaadin.data.util.sqlcontainer.SQLContainer;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.query.TableQuery;

public class SQLContainerFactory {

	private static SQLContainer sqlContainer;

	private static void initDBConn() {
		// TODO Auto-generated method stub
		try {
//			JDBCConnectionPool pool = new SimpleJDBCConnectionPool(
//					"org.h2.Driver", "jdbc:h2:tcp://localhost/~/teststock",
//					"sa", "", 2, 5);
			TableQuery tq = new TableQuery("POSITION", ConnectionPoolHolder.getPool());
			tq.setVersionColumn("OPTLOCK");
			SQLContainer container = new SQLContainer(tq);
			sqlContainer = container;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static SQLContainer getSQLContainer() {
		// TODO Auto-generated method stub
		if (sqlContainer == null) {
			initDBConn();
		}
		return sqlContainer;
	}
}
