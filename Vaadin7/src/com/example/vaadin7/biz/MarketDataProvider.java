/**
 * 
 */
package com.example.vaadin7.biz;

import java.math.BigDecimal;

/**
 * A service to provide the reference market data
 */
public interface MarketDataProvider {
	public BigDecimal getCurrPrice(String stockId);
}
