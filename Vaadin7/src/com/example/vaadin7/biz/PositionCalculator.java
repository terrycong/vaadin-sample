package com.example.vaadin7.biz;

import java.sql.SQLException;
import java.util.List;

public interface PositionCalculator {
	
	/**
	 * Calculate the position with the new BUY trade.
	 * @param newTrade
	 * @throws Exception
	 */
	public void buy(Trade newTrade) throws Exception;
	
	/**
	 * Calculate the position with the new SELL trade.
	 * @param newTrade
	 * @throws Exception
	 */
	public void sell(Trade newTrade) throws Exception;
	
	/**
	 * Return the latest position.
	 * @return
	 * @throws Exception 
	 */
	public List<Position> getLatestPosition() throws Exception;
	
	public Position getExistingPosition(String stockId) throws Exception;
}
