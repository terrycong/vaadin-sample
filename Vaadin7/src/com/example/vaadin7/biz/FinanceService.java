package com.example.vaadin7.biz;

import java.sql.SQLException;

import com.example.vaadin7.dao.ConnectionPoolHolder;

public class FinanceService {
	private PositionCalculator positionCalculator;

	public FinanceService() {
		// TODO Auto-generated constructor stub
		try {
			positionCalculator = new PositionCalculatorImpl(
					ConnectionPoolHolder.getPool().reserveConnection(),
					new MarketDataProviderImpl());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public PositionCalculator getPositionCalculator() {
		return positionCalculator;
	}

	public void setPositionCalculator(PositionCalculator positionCalculator) {
		this.positionCalculator = positionCalculator;
	}

}
