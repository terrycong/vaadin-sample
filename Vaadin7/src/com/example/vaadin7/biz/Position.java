package com.example.vaadin7.biz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class Position {
	private String stockId = null;
	private String stockName = null;
	private BigDecimal buyPrice = null;
	private long quantity = 0;
	private Date buyDate = null;
	private BigDecimal capital = null;
	private BigDecimal currPrice = null;
	private BigDecimal marketValue = null;
	private BigDecimal profit = null;
	private BigDecimal earningRate = null;

	public String getStockId() {
		return stockId;
	}

	public BigDecimal getCapital() {
		return capital;
	}

	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}

	public BigDecimal getCurrPrice() {
		return currPrice;
	}

	public void setCurrPrice(BigDecimal currPrice) {
		this.currPrice = currPrice;
	}

	public BigDecimal getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}

	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	public BigDecimal getEarningRate() {
		return earningRate;
	}

	public void setEarningRate(BigDecimal earningRate) {
		this.earningRate = earningRate;
	}

	public Position(String stockId, String stockName, BigDecimal buyPrice,
			long quantity, Date buyDate, BigDecimal capital,
			BigDecimal currPrice, BigDecimal marketValue, BigDecimal profit,
			BigDecimal earningRate) {
		super();
		this.stockId = stockId;
		this.stockName = stockName;
		this.buyPrice = buyPrice;
		this.quantity = quantity;
		this.buyDate = buyDate;
		this.capital = capital;
		this.currPrice = currPrice;
		this.marketValue = marketValue;
		this.profit = profit;
		this.earningRate = earningRate;
	}

	public Position(String stockId, String stockName, long quantity,
			BigDecimal capital, BigDecimal currPrice) {
		super();
		this.stockId = stockId;
		this.stockName = stockName;
		this.quantity = quantity;
		this.capital = capital;
		this.currPrice = currPrice;
		this.marketValue = currPrice.multiply(new BigDecimal(quantity));
		this.profit = marketValue.subtract(capital);
		this.earningRate = profit.divide(capital, RoundingMode.HALF_UP);
		if (quantity == 0) {
			this.buyPrice = BigDecimal.ZERO;
		} else {
			this.buyPrice = capital.divide(new BigDecimal(quantity),
					RoundingMode.HALF_UP);
		}

	}

	public Position() {
		super();
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

}
