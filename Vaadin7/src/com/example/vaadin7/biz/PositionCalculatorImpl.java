package com.example.vaadin7.biz;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class PositionCalculatorImpl implements PositionCalculator {
	protected static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");

	private final Connection conn;
	private final MarketDataProvider refData;

	public PositionCalculatorImpl(Connection conn, MarketDataProvider refData) {
		super();
		this.conn = conn;
		this.refData = refData;
		try {
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void buy(Trade newTrade) throws Exception {
		Position originalPosition = getExistingPosition(newTrade.getStockId());

		if (originalPosition == null) {
			Position position = new Position(newTrade.getStockId(),
					newTrade.getStockName(), newTrade.getQuantity(),
					newTrade.getAmount(),newTrade.getBuyPrice()
//					refData.getCurrPrice(newTrade
//							.getStockId())
							);
			position.setBuyDate(newTrade.getBuyDate());
			createPosition(position);
		} else {
			Position newPosition = new Position(newTrade.getStockId(),
					newTrade.getStockName(), newTrade.getQuantity()
							+ originalPosition.getQuantity(), newTrade
							.getAmount().add(originalPosition.getCapital()),
							newTrade.getBuyPrice()
//							refData.getCurrPrice(newTrade.getStockId())
					);
			updatePosition(newPosition);
		}
	}

	@Override
	public void sell(Trade newTrade) throws Exception {
		Position originalPosition = getExistingPosition(newTrade.getStockId());

		if (originalPosition == null) {
			throw new Exception(
					"Invalid sell trade. The postion doesn't exist: "
							+ newTrade.getStockId());
		} else {
			Position newPosition = new Position(newTrade.getStockId(),
					newTrade.getStockName(), originalPosition.getQuantity()
							- newTrade.getQuantity(), originalPosition
							.getCapital().subtract(newTrade.getAmount()),
							newTrade.getBuyPrice()
//							refData.getCurrPrice(newTrade.getStockId())
							);
			updatePosition(newPosition);
		}

	}

	@Override
	public List<Position> getLatestPosition() throws Exception {

		final String sql = "select stock_id as stockId, stock_name as stockName, buy_price as buyPrice, quantity, buy_date as buyDate, capital, curr_price as currPrice, market_value as marketValue, profit, earning_rate as earningRate from position order by stock_Id";
		QueryRunner queryRunner = new QueryRunner();
		ResultSetHandler<List<Position>> rsh = new BeanListHandler<Position>(
				Position.class);
		return queryRunner.query(conn, sql, rsh);

	}

	protected void createPosition(Position position) throws SQLException {
		final String sql = "insert into position (stock_id,	stock_name, buy_price, quantity, buy_date, capital, curr_price, market_value, profit, earning_rate) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		QueryRunner queryRunner = new QueryRunner();

		Object[] parms = new Object[] { position.getStockId(),
				position.getStockName(), position.getBuyPrice(),
				position.getQuantity(), position.getBuyDate(),
				position.getCapital(), position.getCurrPrice(),
				position.getMarketValue(), position.getProfit(),
				position.getEarningRate() };

		if (queryRunner.update(conn, sql, parms) != 1)
			throw new SQLException("Failed to insert position data!");
	}

	protected void updatePosition(Position position) throws SQLException {
		final String sql = "update position set buy_price = ?, quantity = ?, capital = ?, curr_price = ?, market_value = ?, profit = ?, earning_rate = ? where stock_id = ?";

		QueryRunner queryRunner = new QueryRunner();

		Object[] parms = new Object[] { position.getBuyPrice(),
				position.getQuantity(), position.getCapital(),
				position.getCurrPrice(), position.getMarketValue(),
				position.getProfit(), position.getEarningRate(),
				position.getStockId() };

		if (queryRunner.update(conn, sql, parms) != 1)
			throw new SQLException("Failed to insert position data!");

	}

	public Position getExistingPosition(String stockId) throws SQLException {
		final String sql = "select stock_id as stockId, stock_name as stockName, buy_price as buyPrice, quantity, buy_date as buyDate, capital, curr_price as currPrice, market_value as marketValue, profit, earning_rate as earningRate from position where stock_id = ?";
		QueryRunner queryRunner = new QueryRunner();
		ResultSetHandler<Position> rsh = new BeanHandler<Position>(
				Position.class);
		return queryRunner.query(conn, sql, rsh, new Object[] { stockId });
	}

}
