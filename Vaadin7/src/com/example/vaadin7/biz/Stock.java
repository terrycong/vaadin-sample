package com.example.vaadin7.biz;

public class Stock {
	private String id;
	private String name;
	private String currentPrice;

	public Stock() {
		// TODO Auto-generated constructor stub
	}

	public Stock(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Stock(String id, String name, String currentPrice) {
		this(id ,name);
		this.currentPrice = currentPrice;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}

}
