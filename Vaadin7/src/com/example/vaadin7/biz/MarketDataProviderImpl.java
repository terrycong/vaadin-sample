/**
 * 
 */
package com.example.vaadin7.biz;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Mock the Ref Data Service to provide the current price for the given stocks.
 */
public class MarketDataProviderImpl implements MarketDataProvider {
	Random random = new Random();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.example2.stock.RefDataService#getCurrPrice(java.lang.String)
	 */
	@Override
	public BigDecimal getCurrPrice(String stockId) {
		if (stockId.equals("000001"))
			return new BigDecimal(80);
		else if (stockId.equals("000002"))
			return new BigDecimal(60);
		else if (stockId.equals("000003"))
			return new BigDecimal(2.5);
		else
			return new BigDecimal(random.nextFloat()*100);
	}

}
