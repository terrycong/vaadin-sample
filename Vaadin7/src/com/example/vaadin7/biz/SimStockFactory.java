package com.example.vaadin7.biz;

import java.util.ArrayList;
import java.util.List;

public class SimStockFactory {
	public static List<Stock> getSimStockList() {
		// TODO Auto-generated method stub
		List<Stock> list = new ArrayList<Stock>();
		list.add(new Stock("AAPL", "苹果", "560.09"));
		list.add(new Stock("XOM", "埃克森美孚", "100.51"));
		list.add(new Stock("GOOG", "谷歌", "1118.4"));
		list.add(new Stock("MSFT", "微软", "39.29"));
		list.add(new Stock("GE", "通用", "21.83"));
		list.add(new Stock("JNJ", "强生", "92.53"));
		return list;
	}
}
